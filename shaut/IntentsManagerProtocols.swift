//
//  IntentsManagerProtocols.swift
//  shauttIntentKit
//
//  Created by Joan Gómez Álvarez on 13/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol AddItemBySearchNameCommandProtocolService: class {
    
    func addItemByNameToSB(itemName: String, shoppingBasketId : String, completionHandler: @escaping (String?, Error?) -> Void)
}
