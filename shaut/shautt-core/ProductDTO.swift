//
//  ProductDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 31/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ProductDTO {
    
    let productId: String
    let productInfo: Dictionary<String,Any>
    var productDataImage: Data?
    var quantity: Int
    
    init(id: String, info: Dictionary<String,Any>, image: Data? = nil, quantity: Int = 0) {
        self.productId = id
        self.productInfo = info
        self.productDataImage = image
        self.quantity = quantity
    }
}
