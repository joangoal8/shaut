//
//  AddItemSBService.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class UpdateItemSBService {
    
    let functions = Functions.functions(region : "europe-west6")
    
    weak var updateItemProductItemDelegate: UpdateItemProductItemDelegate?
    weak var updateItemToShoppingListDelegate: UpdateItemToShoppingListDelegate?
    
    private func updateItemToSB(itemDto: ItemDTO, shoppingBasketId : String, updateRequest: UpdateItemRequest) {
        guard let updateItemRequestData = try? JSONEncoder().encode(updateRequest) else {
            return
        }
        
        guard let dictionaryData = try? JSONSerialization.jsonObject(with: updateItemRequestData, options: .allowFragments) as? [String: Any] else {
            return
        }
        functions.httpsCallable("updateItemSB").call(dictionaryData) { (result, error) in
            if let err = error as NSError? {
                if err.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: err.code)
                    let message = err.localizedDescription
                    let details = err.userInfo[FunctionsErrorDetailsKey]
                    print(code!)
                    print(message)
                    if let detail = details {
                        print(detail)
                    }
                }
                print(err.localizedDescription)
                return
            }
            if let data = (result?.data as? Array<Dictionary<String, Any>>){
                var shopListItems = [ShopItemListDTO]()
                data.forEach { (item) in
                    shopListItems.append(ShopItemListDTO(data: item))
                }
                if let addItemInProductsDelegate = self.updateItemProductItemDelegate {
                    addItemInProductsDelegate.updateItemToShoppingBasket(item: itemDto)
                }
                if let addItemToShoppingBasketDelegate = self.updateItemToShoppingListDelegate {
                    addItemToShoppingBasketDelegate.updateShoppingBasket(items: shopListItems)
                }
            }
        }
    }
    
}

extension UpdateItemSBService: AddItemProtocolService {
    
    func addItemToSB(itemDto: ItemDTO, shoppingBasketId : String) {
        let addItemRequest = UpdateItemRequest(id: shoppingBasketId, itemInfo: ItemInfo(itemId: itemDto.itemId, quantity: itemDto.quantity), operationInfo: OperationInfo(operationAction: OperationAction.ADD))
        self.updateItemToSB(itemDto: itemDto, shoppingBasketId: shoppingBasketId, updateRequest: addItemRequest)
    }
}

extension UpdateItemSBService: RemoveItemProtocolService {
    
    func removeItemToSB(itemDto: ItemDTO, shoppingBasketId : String) {
        let removeItemRequest = UpdateItemRequest(id: shoppingBasketId, itemInfo: ItemInfo(itemId: itemDto.itemId, quantity: itemDto.quantity), operationInfo: OperationInfo(operationAction: OperationAction.REMOVE))
        self.updateItemToSB(itemDto: itemDto, shoppingBasketId: shoppingBasketId, updateRequest: removeItemRequest)
    }
}

extension UpdateItemSBService: EditItemProtocolService {
    
    func editItemToSB(itemDto: ItemDTO, shoppingBasketId : String) {
        let newItemInfo = ItemInfo(itemId: itemDto.itemId, quantity: itemDto.quantity)
        var operationInfo = OperationInfo(operationAction: OperationAction.EDIT)
        operationInfo.newItemInfo = newItemInfo
        let editItemRequest = UpdateItemRequest(id: shoppingBasketId, itemInfo: ItemInfo(itemId: itemDto.itemId, quantity: itemDto.quantity), operationInfo: operationInfo)
        
        self.updateItemToSB(itemDto: itemDto, shoppingBasketId: shoppingBasketId, updateRequest: editItemRequest)
    }
}
