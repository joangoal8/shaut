//
//  UserProfileProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 03/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol UserProfileViewToPresenterInteractorProtocol: class {
    
    func retrieveUserAutoShoppingBasket(userId: String)
    
    func registerDeviceIdToUser(userId: String, deviceId: String)
}

protocol UserProfilePresenterToViewProtocol: class {
    
    func drawAutoShoppingBasketInfo(autoBasketId: String?)
}

protocol UserProfileEntityToPresenterInteractorProtocol {
    
    func provideUserAutoShoppingBasket(autoBasketId: String?)
}
