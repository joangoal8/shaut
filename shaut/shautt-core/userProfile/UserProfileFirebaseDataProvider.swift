//
//  UserProfileFirebaseDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 04/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class UserProfileFirebaseDataProvider {
    
    let dataBase = Firestore.firestore()
    
    let userPresenterProtocol: UserProfileEntityToPresenterInteractorProtocol
    
    init(_ tableViewPresenterInteractor: UserProfileEntityToPresenterInteractorProtocol) {
           self.userPresenterProtocol = tableViewPresenterInteractor
    }
    
    func retrieveUserAutoShoppingBasket(userId: String) {
        let userRef = dataBase.collection("users").document(userId)
        userRef.getDocument { (docSnapshot, error) in
            self.userPresenterProtocol.provideUserAutoShoppingBasket(autoBasketId: docSnapshot?.get("autoShoppingBasketId") as? String)
        }
    }
    
    func registerDeviceInUser(userId: String, deviceId: String) {
        dataBase.collection("shoppingBaskets")
        dataBase.collection("users").document(userId).setData([
            "autoShoppingBasketId": deviceId
        ], merge: true) { error in
            if let err = error {
                print("Error writing document: \(err)")
            } else {
                self.retrieveUserAutoShoppingBasket(userId: userId)
            }
        }
    }
}
