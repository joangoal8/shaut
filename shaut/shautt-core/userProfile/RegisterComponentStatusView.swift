//
//  RegisterComponentStatusView.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 15/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class RegisterComponentStatusView: UIView {

    @IBOutlet weak var statusButton: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
        
    override func draw(_ rect: CGRect) {
        layer.masksToBounds = true
        layer.cornerRadius = layer.frame.height / 3
        statusButton.layer.masksToBounds = true
        statusButton.layer.cornerRadius = 10
    }
    

}
