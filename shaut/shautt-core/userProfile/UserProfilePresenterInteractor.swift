//
//  UserProfilePresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 03/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class UserProfilePresenterInteractor: UserProfileViewToPresenterInteractorProtocol, UserProfileEntityToPresenterInteractorProtocol {
    
    weak var view: UserProfilePresenterToViewProtocol?
    var userFirebaseDataProvider: UserProfileFirebaseDataProvider?
    
    init(_ tableViewController: UserProfilePresenterToViewProtocol) {
        self.view = tableViewController
    }
    
    func retrieveUserAutoShoppingBasket(userId: String) {
        let userDataProvider = getUserProfileFirebaseDataProvider()
        userDataProvider.retrieveUserAutoShoppingBasket(userId: userId)
    }
    
    func registerDeviceIdToUser(userId: String, deviceId: String) {
        let userDataProvider = getUserProfileFirebaseDataProvider()
        userDataProvider.registerDeviceInUser(userId: userId, deviceId: deviceId)
    }
    
    fileprivate func getUserProfileFirebaseDataProvider() -> UserProfileFirebaseDataProvider {
        if let userProfileFirebaseProvider = userFirebaseDataProvider {
            return userProfileFirebaseProvider
        }
        userFirebaseDataProvider = UserProfileFirebaseDataProvider(self)
        return userFirebaseDataProvider!
    }
    
    func provideUserAutoShoppingBasket(autoBasketId: String?) {
        view?.drawAutoShoppingBasketInfo(autoBasketId: autoBasketId)
    }
}
