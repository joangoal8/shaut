//
//  UserProfileViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 18/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import KeychainSwift
import Firebase

class UserProfileViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var backgroundProfileImage: UIImageView!
    
    @IBOutlet weak var welcomeUserMsg: UILabel!
    @IBOutlet weak var registerDeviceIdTxt: UITextField!
    @IBOutlet weak var registerDeviceButtonInView: UIButton!
    
    @IBOutlet weak var registerDeviceView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var logoutBtn: UIButton!
    
    var registerComponentStatusView: RegisterComponentStatusView?
    
    var isProfileImageEdited = false
    var isBackGroundImageEdited = false
    var user: UserLogin?
    var userProfilePresenterInteractor: UserProfilePresenterInteractor?
    
    let firebaseAuth = Auth.auth()
    let imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
        
        if userProfilePresenterInteractor == nil {
            self.userProfilePresenterInteractor = UserProfilePresenterInteractor(self)
        }
        
        updateUserInfo()
        
        registerDeviceButtonInView.layer.cornerRadius = registerDeviceButtonInView.layer.frame.height/2
        registerDeviceButtonInView.layer.masksToBounds = true
    }
    
    fileprivate func updateUserInfo() {
        if let dataUser = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(UserLogin.self, from: dataUser)
                self.user = user
                userProfilePresenterInteractor?.retrieveUserAutoShoppingBasket(userId: user.email)
                welcomeUserMsg.text = "Welcome \(user.email)"
            } catch {
                print(error)
            }
        }
    }
    
    @IBAction func editImageProfile(_ sender: Any) {
        editProfileImage(sender)
    }
    
    private func editProfileImage(_ sender: Any) {
        
        imagePickerController.sourceType = .photoLibrary
        
        imagePickerController.allowsEditing = false
        
        self.present(imagePickerController, animated: true) {
            self.isProfileImageEdited = true
        }
    }
    
    @IBAction func editInfoUserProfile(_ sender: Any) {
        editProfileInfo(sender)
    }
    
    private func editProfileInfo(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        imagePickerController.sourceType = .photoLibrary
        
        imagePickerController.allowsEditing = false
        
        self.present(imagePickerController, animated: true) {
            self.isBackGroundImageEdited = true
        }
    }
    
    @IBAction func registerDevice(_ sender: Any) {
        registerUserDevice(sender)
    }
    
    private func registerUserDevice(_ sender: Any) {
        if registerDeviceIdTxt.text == "" {
            let message = "Please Fill a device Id"
            displayRegisterAlert(message: message)
        } else {
            if let currentUser = user {
                userProfilePresenterInteractor?.registerDeviceIdToUser(userId: currentUser.email, deviceId: registerDeviceIdTxt.text!)
            }
        }
    }
    
    fileprivate func displayRegisterAlert(message: String) {
        let alertController = UIAlertController(title: "Error Register Form", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    //TODO spinners
    func pauseApp() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        activityIndicator.center = view.center
        
        activityIndicator.hidesWhenStopped = true
        
        activityIndicator.style = .medium
        
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        view.isUserInteractionEnabled = false
        
        view.isUserInteractionEnabled = true
        
    }
    
    @IBAction func logoutUser(_ sender: Any) {
        let alertController = UIAlertController(title: "Logout", message: "Estás seguro que quieres cerrar sesión", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
            self.logoutAction()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func logoutAction() {
        KeychainSwift().delete("current_user")
        do {
            try firebaseAuth.signOut()
            switchRootViewController()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    fileprivate func switchRootViewController() {
        self.dismiss(animated: true, completion: nil)
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        if let window = keyWindow {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginNavController = storyboard.instantiateViewController(identifier: "LoginNavigationController")
            window.rootViewController = loginNavController
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imagePicked = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if (isProfileImageEdited) {
                profileImage.image = imagePicked
                profileImage.layer.cornerRadius = profileImage.frame.size.width/2
                profileImage.clipsToBounds = true
            } else if (isBackGroundImageEdited) {
                backgroundProfileImage.image = imagePicked
            }
        } else {
    
            let alertController = UIAlertController(title: "Error Image", message: "Try again later", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            }))
        }
        self.dismiss(animated: true) {
            self.isProfileImageEdited = false
            self.isBackGroundImageEdited = false
        }
    }
}

extension UserProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let orderCell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! UserPreviousOrderTableViewCell
        
        return orderCell
    }
}

extension UserProfileViewController: UserProfilePresenterToViewProtocol {
    
    func drawAutoShoppingBasketInfo(autoBasketId: String?) {
        if autoBasketId != nil {
            if let index = stackView.arrangedSubviews.firstIndex(of: registerDeviceView) {
                if let view = getRegisterComponentStatus() {
                    stackView.insertArrangedSubview(view, at: index)
                }
            }
            registerDeviceView.removeFromSuperview()
        } else {
            print("%%%%%% no shoppingbasketConfigured %%%%%")
        }
    }
    
    fileprivate func getRegisterComponentStatus() -> RegisterComponentStatusView? {
        if let registerCompView = registerComponentStatusView {
            return registerCompView
        }
        registerComponentStatusView = Bundle.main.loadNibNamed("RegisterComponentView", owner: nil, options: nil)?[0] as? RegisterComponentStatusView
        return registerComponentStatusView
    }
}
