//
//  AddItemToShoppingBasketView.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 15/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class AddItemToShoppingBasketView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var itemImage: UIImageView!
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var hStackAmount: UIStackView!
    
    @IBOutlet weak var addToShoppingBktBtn: UIButton!
    @IBOutlet weak var amountLabel: UILabel!

    var itemPresenterInteractor: ProductItemPresenterInteractor?
    var shopListPresenterInteractor: ShopListPresenterInteractor?
    
    var itemDto: ItemDTO?
    var isEditable: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        self.frame = UIScreen.main.bounds
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     */
    override func draw(_ rect: CGRect) {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animateOut)))
        containerView.frame = UIScreen.main.bounds
        hStackAmount.layer.masksToBounds = true
        hStackAmount.layer.cornerRadius = layer.frame.height / 3
        addToShoppingBktBtn.layer.masksToBounds = true
        addToShoppingBktBtn.layer.cornerRadius = 10
        animateIn()
    }
    
    @objc fileprivate func animateOut() {
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.containerView.transform = CGAffineTransform(translationX: 0, y: +self.frame.height)
        }) { (complete) in
            if complete {
                self.removeFromSuperview()
            }
        }
    }
    
    @objc fileprivate func animateIn() {
        self.containerView.transform = CGAffineTransform(translationX: 0, y: +self.frame.height)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.containerView.transform = .identity
        })
    }
    
    func setUpViewWithItemDTO(presenter: ProductItemPresenterInteractor, itemDTO: ItemDTO) {
        self.itemDto = itemDTO
        self.itemPresenterInteractor = presenter
        commonSetup(itemDTO)
    }
    
    fileprivate func commonSetup(_ itemDTO: ItemDTO) {
        if let dataImage = itemDTO.itemDataImage {
            itemImage.image = UIImage(data: dataImage)!
        } else {
            itemImage.image = UIImage(named: "noImage")!
        }
        itemNameLabel.text = itemDTO.name_es
        itemNameLabel.numberOfLines = 2
        amountLabel.text = "\(itemDTO.quantity)"
    }
    
    func setUpEditableViewWithItemDTO(presenter: ShopListPresenterInteractor, itemDTO: ItemDTO) {
        self.itemDto = itemDTO
        self.shopListPresenterInteractor = presenter
        commonSetup(itemDTO)
        self.isEditable = true
        self.addToShoppingBktBtn.setTitle("Editar", for: .normal)
    }
    
    @IBAction func closeView(_ sender: Any) {
        animateOut()
    }
    
    @IBAction func minusOne(_ sender: Any) {
        guard var item = itemDto else {
            return
        }
        
        if (item.quantity > 0) {
            item.quantity -= 1
            itemDto?.quantity = item.quantity
            amountLabel.text = "\(item.quantity)"
        }
    }
    
    @IBAction func plusOne(_ sender: Any) {
        guard var item = itemDto else {
            return
        }
        
        item.quantity += 1
        itemDto?.quantity = item.quantity
        amountLabel.text = "\(item.quantity)"
    }
    
    @IBAction func addItemToShoppingBasket(_ sender: Any) {
        if isEditable {
            if let shopListPresentInt = shopListPresenterInteractor {
                shopListPresentInt.editItemShoppingList(item: itemDto!)
            }
        } else {
            itemPresenterInteractor!.addItemToShoppingBasket(item: itemDto!)
        }
        
        animateOut()
    }
}
