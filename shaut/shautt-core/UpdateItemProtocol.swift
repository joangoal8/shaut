//
//  AddItemProtocol.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol AddItemProtocolService: class {
    
    func addItemToSB(itemDto: ItemDTO, shoppingBasketId : String)
}

protocol RemoveItemProtocolService: class {
    
    func removeItemToSB(itemDto: ItemDTO, shoppingBasketId : String)
}

protocol EditItemProtocolService: class {
    
    func editItemToSB(itemDto: ItemDTO, shoppingBasketId : String)
}

protocol UpdateItemProductItemDelegate: class {
    
    func updateItemToShoppingBasket(item: ItemDTO)
}

protocol UpdateItemToShoppingListDelegate: class {
    
    func updateShoppingBasket(items: [ShopItemListDTO])
}

