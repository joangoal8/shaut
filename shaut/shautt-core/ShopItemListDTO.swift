//
//  ShopItemListDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 25/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ShopItemListDTO: Decodable {
    
    let itemId: String
    let categoryId : String
    let quantity: Int
    let name: String
    let name_es: String
    var dietType: String?
    var deliberryMercadonaItemId: String?
    var deliberryCapraboItemId: String?
    var deliberryDiaItemId: String?
    var lolamarketItemId: String?
    var itemDataImage: Data?
    
    init(data: Dictionary<String,Any>, image: Data? = nil) {
        self.itemId = data["itemId"] as! String
        self.categoryId = data["categoryId"] as! String
        self.quantity = data["quantity"] as! Int
        self.name = data["name"] as! String
        self.name_es = data["name_es"] as! String
        if let diet = data["dietType"] {
            self.dietType = diet as? String
        }
        if let deliberryProductId = data["deliberryItemId"] {
            self.deliberryMercadonaItemId = deliberryProductId as? String
        }
        if let deliberryProductId = data["deliberryCapraboItemId"] {
            self.deliberryCapraboItemId = deliberryProductId as? String
        }
        if let deliberryProductId = data["deliberryDiaItemId"] {
            self.deliberryDiaItemId = deliberryProductId as? String
        }
        if let lolamarketProductId = data["lolamarketItemId"] {
            self.lolamarketItemId = lolamarketProductId as? String
        }
        self.itemDataImage = image
    }
    
    enum CodingKeys: String, CodingKey {
        case itemId, categoryId, quantity, name, name_es, dietType
    }
}
