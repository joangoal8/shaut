//
//  ProviderInitConfigurationManager.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase
import KeychainSwift

class ProviderInitConfigurationManager {
    
    let dataBase = Firestore.firestore()
    
    func configProviders() {
        retrieveStoredConfigProviders()
    }
    
    private func retrieveStoredConfigProviders() {
        dataBase.collection("providers").getDocuments() { (snapshot, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
            }
            
            var shopProviders = [ShopProviderDTO]()
            if let snapshotResult = snapshot {
                for document in snapshotResult.documents {
                    let shopProvider = ShopProviderDTO(data: document.data() as Dictionary<String,Any>)
                    shopProviders.append(shopProvider)
                }
                self.storeProvidersKeychain(shopProviders)
            }
            
        }
    }
    
    fileprivate func storeProvidersKeychain(_ shopProviders: [ShopProviderDTO]) {
        if let shopProvidersEncoded = try? JSONEncoder().encode(shopProviders) {
            KeychainSwift().set(shopProvidersEncoded, forKey: "shopping_providers")
        }
    }
    
    func retrieveProviders(completionHandler: @escaping ([ShopProviderDTO]) -> Void ) {
        var shopProviders = [ShopProviderDTO]()
        
        dataBase.collection("providers").getDocuments() { (snapshot, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
            }
            
            if let snapshotResult = snapshot {
                for document in snapshotResult.documents {
                    let shopProvider = ShopProviderDTO(data: document.data() as Dictionary<String,Any>)
                    shopProviders.append(shopProvider)
                }
                
                completionHandler(shopProviders)
            }
        }
    }
}
