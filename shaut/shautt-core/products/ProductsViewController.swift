//
//  ProductsViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var productPresenterInteractor: ProductPresenterInteractor?
    var products = [ProductDTO]()
    var filteredProducts = [ProductDTO]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var customTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if productPresenterInteractor == nil {
            self.productPresenterInteractor = ProductPresenterInteractor(self)
        }
        
        self.productPresenterInteractor?.retrieveProducts()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.searchBar.text != nil && self.searchBar.text != "") {
            return self.filteredProducts.count
        }
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productList = (self.searchBar.text != nil && self.searchBar.text != "") ? self.filteredProducts : self.products
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductTableViewCell
        
        let productDto = productList[indexPath.row]
        
        cell.title.text = productDto.productInfo["title"] as? String
        cell.descriptionTxt.text = productDto.productInfo["description"] as? String
        let price = productDto.productInfo["price"] as! NSNumber
        cell.priceText.text = "\(price.decimalValue) €"
        if let dataImage = productDto.productDataImage {
            cell.productImage.image = UIImage(data: dataImage)!
        } else {
            cell.productImage.image = UIImage(named: "noImage")!
        }
        
        return cell
    }
}

extension ProductsViewController: ProductPresenterToViewProtocol {
    func drawProductsTable(products: [ProductDTO]) {
        self.products = products
        self.customTableView.reloadData()
    }
}

extension ProductsViewController:  UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("changing")
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
