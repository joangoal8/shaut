//
//  ProductPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 31/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class ProductPresenterInteractor: ProductViewToPresenterInteractorProtocol, ProductEntityToPresenterInteractorProtocol {
    
    var view: ProductPresenterToViewProtocol?
    var products = [ProductDTO]()
    var images = [String:Data]()
    var productFireStoreDataProvider: ProductFireStoreDataProvider?
    
    init(_ tableViewController: ProductPresenterToViewProtocol) {
        self.view = tableViewController
    }
    
    func retrieveProducts() {
        let productFireStore = getProductFireStoreDataProvider()
        productFireStore.retrieveProducts()
    }
    
    func presentProductsTable(products: [ProductDTO]) {
        self.products = products
        view?.drawProductsTable(products: self.products)
    }
    
    func addImages(imageId: String, data: Data?) {
        images[imageId] = data
        if (products.count > 0 && products.count == images.count) {
            var productsWithImages = [ProductDTO]()
            for i in 0...products.count - 1 {
                var product = products[i]
                product.productDataImage = images[product.productId]
                productsWithImages.append(product)
            }
            self.products = productsWithImages
            view?.drawProductsTable(products: products)
        }
    }
    
    fileprivate func getProductFireStoreDataProvider() -> ProductFireStoreDataProvider {
        if let productFireStore = productFireStoreDataProvider {
            return productFireStore
        }
        productFireStoreDataProvider = ProductFireStoreDataProvider(self)
        return productFireStoreDataProvider!
    }
}
