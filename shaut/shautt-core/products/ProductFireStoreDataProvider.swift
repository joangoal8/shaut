//
//  ProductFireStoreDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 30/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class ProductFireStoreDataProvider {
    
    let dataBase = Firestore.firestore()
    let storage = Storage.storage()
    let productEntityToPresenterInteractorProtocol: ProductEntityToPresenterInteractorProtocol
    
    init(_ tableViewPresenterInteractor: ProductEntityToPresenterInteractorProtocol) {
        self.productEntityToPresenterInteractorProtocol = tableViewPresenterInteractor
    }
    
    func retrieveProducts() {
        var products = [ProductDTO]()
        dataBase.collection("products").getDocuments() { (snapshot, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
            }
            
            for document in snapshot!.documents {
                
                let imageRef = self.storage.reference().child(
                "products/images/\(document.documentID).jpg")
                imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    
                    if let errorResult = error {
                        print(errorResult.localizedDescription)
                        self.productEntityToPresenterInteractorProtocol.addImages(imageId: document.documentID, data: nil)
                        return
                    }
                    self.productEntityToPresenterInteractorProtocol.addImages(imageId: document.documentID, data: data!)
                    
                }
                let product = ProductDTO(id: document.documentID, info: document.data() as Dictionary<String,Any>)
                products.append(product)
            }
            self.productEntityToPresenterInteractorProtocol.presentProductsTable(products: products)
        }
    }
}
