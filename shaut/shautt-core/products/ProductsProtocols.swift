//
//  ProductsProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 31/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

protocol ProductViewToPresenterInteractorProtocol: class {
    
    var view: ProductPresenterToViewProtocol? {get set}
    
    func retrieveProducts()
}

protocol ProductPresenterToViewProtocol {
    
    func drawProductsTable(products: [ProductDTO])
}

protocol ProductEntityToPresenterInteractorProtocol {
    
    func presentProductsTable(products: [ProductDTO])
    
    func addImages(imageId: String, data: Data?)
}
