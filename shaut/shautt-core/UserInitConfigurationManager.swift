//
//  UserInitConfigurationManager.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 04/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class UserInitConfigurationManager {
    
    let dataBase = Firestore.firestore()
    var keyChainManager: KeyChainManager?
    let userDefaults = UserDefaults(suiteName: ShauttConstants.GROUP_ID)
    
    func configureUserWithShoppingBasket(userId: String, auth: String, providerLogin: LoginProvider) {
        addUserAndLinkNewShoppingBasket(userId)
        getKeychainManager().storeUserInKeychain(id: userId, auth: auth, providerLogin: providerLogin)
    }
    
    fileprivate func addUserAndLinkNewShoppingBasket(_ userId: String) {
        var shoppingBasketRef: DocumentReference?
        shoppingBasketRef = dataBase.collection("shoppingBaskets").addDocument(data: [:]) { error in
            if let err = error {
                print("Error adding document: \(err)")
            } else {
                self.dataBase.collection("users").document(userId).setData([
                    "shoppingBasketId": shoppingBasketRef!.documentID
                ], merge: true) { error in
                    if let err = error {
                        print("Error writing document: \(err)")
                    } else {
                        print("Everything well configured Document added with ID:")
                    }
                }
                self.getKeychainManager().storeShoppingBasketIdInKeyChain(shoppingBasketId: shoppingBasketRef!.documentID)
                if let defaults = self.userDefaults {
                    defaults.set(shoppingBasketRef!.documentID, forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY)
                }
            }
        }
    }
    
    fileprivate func getKeychainManager() -> KeyChainManager {
        if let keyChainM = keyChainManager {
            return keyChainM
        }
        keyChainManager = KeyChainManager()
        return keyChainManager!
    }
    
    func configureSocialUserWithShoppingBasket(userId: String, auth: String, providerLogin: LoginProvider) {
        
        let userRef = dataBase.collection("users").document(userId)

        userRef.getDocument { (document, error) in
            if let document = document, document.exists {
                print("The social user is already configured with the following data data: \(String(describing: document.data()))")
            } else {
                self.addUserAndLinkNewShoppingBasket(userId)
            }
        }
        getKeychainManager().storeUserInKeychain(id: userId, auth: auth, providerLogin: providerLogin)
    }
    
    func configureLoginUserConfiguration(userId: String, auth: String, providerLogin: LoginProvider) {
        let userRef = dataBase.collection("users").document(userId)

        userRef.getDocument { (document, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
                return
            }
            if let doc = document, let dataDict = doc.data() {
                let shoppingBasketId = dataDict["shoppingBasketId"] as! String
                self.getKeychainManager().storeShoppingBasketIdInKeyChain(shoppingBasketId: shoppingBasketId)
                if let defaults = self.userDefaults {
                    defaults.set(shoppingBasketId, forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY)
                }
            }
        }
        getKeychainManager().storeUserInKeychain(id: userId, auth: auth, providerLogin: providerLogin)
    }
}
