//
//  KeyChainManager.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 15/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import KeychainSwift

class KeyChainManager {
    
    var keyChainSwift: KeychainSwift?
    
    func storeUserInKeychain(id: String, auth: String, providerLogin: LoginProvider) {
        switch providerLogin {
        case .PasswordEmail:
            let userPwd = EmailPwdLoginUser(email: id, auth: auth)
            let encoder = JSONEncoder()
            if let passwordUserEncoded = try? encoder.encode(userPwd) {
                getKeychainSwift().set(passwordUserEncoded, forKey: "current_user")
                print("### user stored: \(passwordUserEncoded)")
            }
            break
        case .Google, .Facebook, .Apple:
            let googleUser = SocialLoginUser(email: id, auth: auth, providerName: .Google)
            let encoder = JSONEncoder()
            if let googleUserEncoded = try? encoder.encode(googleUser) {
                getKeychainSwift().set(googleUserEncoded, forKey: "current_user")
                print("### user stored: \(googleUserEncoded)")
            }
            break
        }
    }
    
    fileprivate func getKeychainSwift() -> KeychainSwift {
        if let keyChain = keyChainSwift {
            return keyChain
        }
        keyChainSwift = KeychainSwift()
        return keyChainSwift!
    }
    
    func storeShoppingBasketIdInKeyChain(shoppingBasketId: String) {
        getKeychainSwift().set(shoppingBasketId, forKey: ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY)
    }
    
}
