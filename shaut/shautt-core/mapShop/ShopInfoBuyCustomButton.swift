//
//  ShopInfoBuyCustomButton.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ShopInfoBuyCustomButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpButton()
    }
    
    func configure(title: String) {
        setTitle("Buy", for: .normal)
    }
    
    private func setUpButton() {
        setTitleColor(.white, for: .normal)
        
        backgroundColor = UIColor.systemPink
        titleLabel?.font = UIFont(name: "System", size: 15)
        layer.cornerRadius = 21
        setShadow()
    }
    
    private func setShadow() {
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowRadius = 8
        layer.shadowOpacity = 0.3
        clipsToBounds = true
        layer.masksToBounds = false
    }

    func shake() {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 3, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 3, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "postion")
    }
}
