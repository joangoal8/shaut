//
//  ShopMarker.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import GoogleMaps

class ShopMarker: GMSMarker {

    let place: GooglePlace
    
    init(place: GooglePlace, iconImage: UIImage) {
        self.place = place
        super.init()
        
        position = place.coordinate
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
        
        icon = iconImage
    }
}

class ShopMarkerImageMapper {
    
    func mapNameShopProviderToIconImage(shopProviderName: String) -> UIImage {
        let name = shopProviderName.uppercased()
        switch name {
        case ShopMarkerProviderIconType.MERCADONA.rawValue:
            return UIImage(named: "mercadonaLogo")!
        case ShopMarkerProviderIconType.CAPRABO.rawValue:
            return UIImage(named: "capraboLogo")!
        case ShopMarkerProviderIconType.DIA.rawValue:
            return UIImage(named: "diaLogo")!
        case ShopMarkerProviderIconType.LIDL.rawValue:
            return UIImage(named: "lidlLogo")!
        case ShopMarkerProviderIconType.SPAR.rawValue:
            return UIImage(named: "sparLogo")!
        default:
            if (name.contains(ShopMarkerProviderIconType.VEGAN.rawValue)) {
                return UIImage(named: "veganLogo")!
            }
            if (name.contains(ShopMarkerProviderIconType.ECO.rawValue)) {
                return UIImage(named: "ecoLogo")!
            }
            return UIImage(named: "shopDefaultLogo")!
        }
    }
}

enum ShopMarkerProviderIconType: String {
    case MERCADONA
    case CAPRABO
    case DIA
    case LIDL
    case SPAR
    case VEGAN
    case ECO
}
