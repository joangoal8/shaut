//
//  MapShopViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON

class MapShopViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    private let locationManager = CLLocationManager()
    
    var mapShopPresenterInteractor: MapShopViewToPresenterInteractorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
          
        if CLLocationManager.locationServicesEnabled() {
          locationManager.requestLocation()
          mapView.isMyLocationEnabled = true
          mapView.settings.myLocationButton = true
        } else {
          locationManager.requestWhenInUseAuthorization()
        }
        
        mapView.delegate = self
    }
    
    @IBAction func changeLocation(_ sender: Any) {
        
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        
        // selected location
        
        self.locationManager.stopUpdatingLocation()
        
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func retrieveShopProviders(location: CLLocation) {
        if mapShopPresenterInteractor == nil {
            self.mapShopPresenterInteractor = MapShopPresenterInteractor(self)
        }
        
        mapShopPresenterInteractor?.fetchShopProvidersPlaces(coordinate: location.coordinate)
    }

}

// MARK: - CLLocationManagerDelegate
extension MapShopViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      guard status == .authorizedWhenInUse else {
        return
      }
      
      locationManager.requestLocation()
      mapView.isMyLocationEnabled = true
      mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        mapView.camera = GMSCameraPosition(
            target: location.coordinate,
            zoom: 15,
            bearing: 0,
            viewingAngle: 0)
        
        retrieveShopProviders(location: location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      print(error)
    }
    
}

// MARK: - GMSMapViewDelegate
extension MapShopViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
      print(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
      
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let shopMarker = marker as? ShopMarker else {
          return nil
        }

        let infoView = ShopMarkerInfoView()
        
        infoView.nameLabel.text = shopMarker.place.name
        infoView.addressLabel.text = shopMarker.place.address
        if let opening = shopMarker.place.openingHours {
            infoView.isOpenImageView.image = opening.isOpenNow ? UIImage(named: "openLogo") : UIImage(named: "closedLogo")
        }
        
        self.view.addSubview(infoView)
        
        return nil
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
      return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
      mapView.selectedMarker = nil
      return false
    }
}

// MARK: - GMSAutocompleteViewControllerDelegate
extension MapShopViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        self.mapView.camera = GMSCameraPosition(
            target: place.coordinate,
        zoom: 15,
        bearing: 0,
        viewingAngle: 0)
        dismiss(animated: true, completion: nil)
        
        let selectedLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        retrieveShopProviders(location: selectedLocation)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension MapShopViewController: MapShopPresenterToViewProtocol {
    
    func drawShopPlacesMarkers(shopMarkers: [ShopMarker]) {
        mapView.clear()
        
        shopMarkers.forEach { (shopMarker) in
            shopMarker.map = self.mapView
        }
    }
    
}
