//
//  MapShowProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GoogleMaps

protocol MapShopViewToPresenterInteractorProtocol: class {
    
    func fetchShopProvidersPlaces(coordinate: CLLocationCoordinate2D)
}

protocol MapShopPresenterToViewProtocol: class {
    
    func drawShopPlacesMarkers(shopMarkers: [ShopMarker])
}

protocol MapShopEntityToPresenterInteractorProtocol {
    
    func provideShopPlaces(places: [GooglePlace])
}
