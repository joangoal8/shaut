//
//  MapShopPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GoogleMaps
import KeychainSwift

class MapShopPresenterInteractor: MapShopViewToPresenterInteractorProtocol {
    
    let shopMarkerImageMapper = ShopMarkerImageMapper()
    weak var view: MapShopPresenterToViewProtocol?
    var shopProviders = [ShopProviderDTO]()
    
    var googlePlaceDataProvider: GooglePlaceDataProvider?
    
    init(_ view: MapShopViewController) {
        self.view = view
    }
    
    func fetchShopProvidersPlaces(coordinate: CLLocationCoordinate2D) {
        let shopProviderData = self.getGooglePlaceDataProvider()
        
        if let shoppingProvidersStored = KeychainSwift().getData("shopping_providers") {
            do {
                let decoder = JSONDecoder()
                let shoppingProviders = try decoder.decode([ShopProviderDTO].self, from: shoppingProvidersStored)
                print("%&$&&%$$& Printo Providers Stored")
                print(shoppingProviders)
                shopProviders = shoppingProviders
                
            } catch {
                print("ERROR")
                print(error.localizedDescription)
            }
        }
        
        shopProviderData.fetchPostalCodeLocation(coordinate: coordinate) { (addressComponents) in
            
            let postalCodeAddressComp = addressComponents.filter { (addressComp) -> Bool in
                return addressComp.types[0] == "postal_code"
            }.map { (addressComp) -> String in
                return addressComp.longName
            }.first
            
            if let postalCode = postalCodeAddressComp {
                KeychainSwift().set(postalCode, forKey: "postal_code")
            }
        }
        shopProviderData.fetchProvidersLocation(coordinate: coordinate, radius: 2000)
    }
    
    fileprivate func getGooglePlaceDataProvider() -> GooglePlaceDataProvider {
        if let googlePlaceProvider = googlePlaceDataProvider {
            return googlePlaceProvider
        }
        googlePlaceDataProvider = GooglePlaceDataProvider(self)
        return googlePlaceDataProvider!
    }
}

extension MapShopPresenterInteractor: MapShopEntityToPresenterInteractorProtocol {
    
    func provideShopPlaces(places: [GooglePlace]) {
        var shopMarkers = [ShopMarker]()
        places.forEach { (place) in
            shopMarkers.append(ShopMarker(place: place, iconImage: shopMarkerImageMapper.mapNameShopProviderToIconImage(shopProviderName: place.name)))
        }
        view?.drawShopPlacesMarkers(shopMarkers: shopMarkers)
    }
    
}
