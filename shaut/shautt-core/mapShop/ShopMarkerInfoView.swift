//
//  ShopMarkerInfoView.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ShopMarkerInfoView: UIView {
    
    var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 21, weight: .bold)
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    var addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.numberOfLines = 3
        label.textAlignment = .justified
        return label
    }()
    
    var isOpenImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    fileprivate let buyButton = ShopInfoBuyCustomButton()
    
    fileprivate lazy var hStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [addressLabel, isOpenImageView])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.spacing = 15
        return stack
    }()
    
    fileprivate lazy var vStack: UIStackView = {
        buyButton.configure(title: "Buy")
        let stack = UIStackView(arrangedSubviews: [nameLabel, hStack, buyButton])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.spacing = 40
        return stack
    }()
    
    fileprivate let container: UIView = {
        let viewContainer = UIView()
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        viewContainer.backgroundColor = .white
        viewContainer.layer.cornerRadius = 21
        return viewContainer
    }()
    
    @objc fileprivate func animateOut() {
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.container.transform = CGAffineTransform(translationX: 0, y: +self.frame.height)
        }) { (complete) in
            if complete {
                self.removeFromSuperview()
            }
        }
    }
    
    @objc fileprivate func animateIn() {
        self.container.transform = CGAffineTransform(translationX: 0, y: +self.frame.height)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.container.transform = .identity
        })
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addActionToButton()
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animateOut)))
        self.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        self.frame = UIScreen.main.bounds
        self.addSubview(container)
        
        container.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        container.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.7).isActive = true
        container.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4).isActive = true
        
        container.addSubview(vStack)

        vStack.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        vStack.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        vStack.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: 0.8).isActive = true
        vStack.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.8).isActive = true
        
        buyButton.center.x = vStack.center.x
        buyButton.heightAnchor.constraint(equalTo: vStack.heightAnchor, multiplier: 0.15).isActive = true
        buyButton.widthAnchor.constraint(equalTo: vStack.widthAnchor).isActive = true
        
        animateIn()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func addActionToButton() {
        buyButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    @objc func buttonClicked() {
        buyButton.shake()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            self.removeFromSuperview()
        }
    }
}
