//
//  GeocodingPostalCodeDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct AddressComponentsDTO: Codable {
    let longName: String
    let shortName: String
    let types: [String]
    
    enum CodingKeys: String, CodingKey {
        case longName = "long_name"
        case shortName = "short_name"
        case types
    }
    
}

extension AddressComponentsDTO {
    struct Response: Codable {
        let results: [GeocodeInfo]
    }
    
    struct GeocodeInfo: Codable {
        let addressComonents: [AddressComponentsDTO]
        
        enum CodingKeys: String, CodingKey {
            case addressComonents = "address_components"
        }
    }
    
}
