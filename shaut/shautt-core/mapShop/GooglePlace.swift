//
//  GooglePlace.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import CoreLocation

struct GooglePlace: Codable {
    let placeId: String
    let name: String
    let address: String
    let openingHours: OpeningHours?
  
    private let geometry: Geometry
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: geometry.location.lat, longitude: geometry.location.lng)
    }

    enum CodingKeys: String, CodingKey {
        case placeId = "place_id"
        case name
        case address = "vicinity"
        case geometry
        case openingHours = "opening_hours"
    }
}

extension GooglePlace {
    struct Response: Codable {
        let results: [GooglePlace]
        let status: String?
    }
  
    private struct Geometry: Codable {
        let location: Coordinate
    }
  
    private struct Coordinate: Codable {
        let lat: CLLocationDegrees
        let lng: CLLocationDegrees
    }
    
    struct OpeningHours: Codable {
        let isOpenNow: Bool
        
        enum CodingKeys: String, CodingKey {
            case isOpenNow = "open_now"
        }
    }
}
