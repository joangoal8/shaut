//
//  GooglePlaceDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GooglePlaces

class GooglePlaceDataProvider {
    
    let presenterInteractor: MapShopEntityToPresenterInteractorProtocol
    
    private var placesTask: URLSessionDataTask?
    private var session: URLSession {
      return URLSession.shared
    }
    private var placesClient: GMSPlacesClient {
        return GMSPlacesClient.shared()
    }
    
    init(_ presenterInteractor: MapShopEntityToPresenterInteractorProtocol) {
        self.presenterInteractor = presenterInteractor
    }
    
    func fetchProvidersLocation(coordinate: CLLocationCoordinate2D, radius: Int) {
        let semaphore = DispatchSemaphore (value: 0)
        var googleResponse: GooglePlace.Response?
        let urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)&type=grocery_or_supermarket&key=\(googleApiKey)"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                print(String(describing: error))
                return
            }
            do {
                googleResponse = try JSONDecoder().decode(GooglePlace.Response.self, from: data)
            } catch {
                print("ERRROR Getting all the places")
                print(error.localizedDescription)
            }
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
    
        if let response = googleResponse {
            self.presenterInteractor.provideShopPlaces(places: response.results)
        }
    }
    
    func fetchPostalCodeLocation(coordinate: CLLocationCoordinate2D, completionHandler: @escaping ([AddressComponentsDTO]) -> Void) {
        
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(coordinate.latitude),\(coordinate.longitude)&key=\(googleApiKey)"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                print(String(describing: error))
                return
            }
            do {
                let resultsAddressComponents = try JSONDecoder().decode(AddressComponentsDTO.Response.self, from: data)
                completionHandler(resultsAddressComponents.results[0].addressComonents)
            } catch {
                print("ERRROR Getting all the places")
                print(error.localizedDescription)
            }
            
        }
        task.resume()
    }
}
