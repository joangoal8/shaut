//
//  RegisterViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 20/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {

    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    let userInitConfigurationManager = UserInitConfigurationManager()
    
    @IBAction func register(_ sender: Any) {
        if emailText.text == "" || passwordText.text == "" {
            let message = "Please Fill Email and password"
            displayRegisterAlert(message: message)
        } else {
            Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!) {
                (authResult, error) in
                if error == nil {
                    
                    guard let userUuid = authResult?.user.uid else {
                        print("error getting the uuid")
                        return
                    }
                    self.userInitConfigurationManager.configureUserWithShoppingBasket(userId: userUuid, auth: self.passwordText.text!, providerLogin: .PasswordEmail)
                    self.switchToMenuViewController()
                } else if let resErr = error {
                    print("$$$$ ERROR Specified \(resErr.localizedDescription)")
                    self.displayRegisterAlert(message: resErr.localizedDescription)
                }
            }
        }
    }
    
    fileprivate func displayRegisterAlert(message: String) {
        let alertController = UIAlertController(title: "Error Register Form", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func switchToMenuViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MenuTabBarController")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerButton.layer.cornerRadius = registerButton.layer.frame.height/2
        registerButton.layer.masksToBounds = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
