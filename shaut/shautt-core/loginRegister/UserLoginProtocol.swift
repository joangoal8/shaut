//
//  UserLoginProtocol.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 27/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol UserLoginProtocol: Encodable {
    
    var email: String {get}
    var auth: String {get}
    var providerName: LoginProvider {get}
}

struct UserLogin: Decodable {
    
    let email: String
    let auth: String
    let providerName: LoginProvider
    
    init(email: String, auth: String, providerName: LoginProvider) {
        self.email = email
        self.auth = auth
        self.providerName = providerName
    }
}
