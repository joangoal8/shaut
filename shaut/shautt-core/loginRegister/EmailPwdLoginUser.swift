//
//  EmailPwdLoginUser.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 27/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct EmailPwdLoginUser: UserLoginProtocol {
    
    var email: String
    var auth: String
    var providerName: LoginProvider
    
    init(email: String, auth: String, providerName: LoginProvider = .PasswordEmail) {
        self.email = email
        self.auth = auth
        self.providerName = providerName
    }
}
