//
//  SignInViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 19/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

class SignInViewController: UIViewController {

    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    @IBOutlet weak var notRegisteredLabel: UILabel!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    @IBAction func loginUser(_ sender: Any) {
       
        if emailText.text == "" || passwordText.text == "" {
            displayRegisterAlert(message: "Please fill an Email and password")
        } else {
            Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!) { [weak self] authResult, error in
                guard let strongSelf = self else { return }
                if error == nil {
                    UserInitConfigurationManager().configureLoginUserConfiguration(userId: strongSelf.emailText.text!, auth: strongSelf.passwordText.text!, providerLogin: .PasswordEmail)
                    strongSelf.switchToMenuViewController()
                } else if let resErr = error {
                    print("$$$$ ERROR Specified \(resErr.localizedDescription)")
                    strongSelf.displayRegisterAlert(message: resErr.localizedDescription)
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerButton.layer.cornerRadius = registerButton.layer.frame.height/2
        registerButton.layer.masksToBounds = true
        
        logInButton.layer.cornerRadius = logInButton.layer.frame.height/2
        logInButton.layer.borderColor = UIColor.red.cgColor
        logInButton.layer.borderWidth = 1
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.presentingViewController = self
    
        /*
        let facebookLoginButton = FBLoginButton()
        facebookLoginButton.center.y = googleSignInButton.center.y + googleSignInButton.frame.height + 15
        facebookLoginButton.center.x = googleSignInButton.center.x
        
        view.addSubview(facebookLoginButton)
        
        if let token = AccessToken.current,
            !token.isExpired {
            print("User logged FACEBOOK")
        }
        */
    }
    
    fileprivate func displayRegisterAlert(message: String) {
        let alertController = UIAlertController(title: "Error Login", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func switchToMenuViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MenuTabBarController")
               
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
