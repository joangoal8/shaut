//
//  FeedNewPostViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 20/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import DropDown

class FeedNewPostViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var newReceipeImageBtn: UIButton!
    
    @IBOutlet weak var newReceipeImage: UIImageView!
    
    @IBOutlet weak var receipeTitle: UITextField!
    
    @IBOutlet weak var newReceipeDescriptionText: UITextView!
    
    @IBOutlet weak var categoryTxtField: UITextField!
    
    @IBOutlet weak var receipeItemsCollection: UICollectionView!
    
    @IBOutlet weak var publishButton: UIButton!
    
    let testDropdown = ["modify", "test2", "category1", "isScroll", "fitness"]
    
    let menu = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        newReceipeImage.contentMode = .scaleAspectFit
        newReceipeImage.layer.cornerRadius = newReceipeImage.frame.height / 10
        
        newReceipeDescriptionText.layer.borderColor = UIColor.lightGray.cgColor
        newReceipeDescriptionText.layer.borderWidth = 1.0
        newReceipeDescriptionText.layer.cornerRadius = newReceipeDescriptionText.frame.height / 8
        
        publishButton.layer.cornerRadius = publishButton.frame.height / 2
        // Do any additional setup after loading the view.
        
        self.menu.dataSource = testDropdown
        self.menu.anchorView = categoryTxtField
        self.menu.backgroundColor = .white
        self.menu.textColor = .systemPink
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(defineCategory))
        gesture.numberOfTouchesRequired = 1
        
        categoryTxtField.addGestureRecognizer(gesture)
        
        self.menu.selectionAction = {index, title in
            self.categoryTxtField.text = self.menu.selectedItem
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func storeImage(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        imagePickerController.sourceType = .photoLibrary
        
        imagePickerController.allowsEditing = true
        
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func defineCategory() {
        menu.show()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

extension FeedNewPostViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension FeedNewPostViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imagePicked = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.newReceipeImage.contentMode = .scaleAspectFit
            self.newReceipeImage.image = imagePicked
        } else {
            let alertController = UIAlertController(title: "Error Image", message: "Try again later", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            }))
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension FeedNewPostViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let receipeItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReceipeItemCell", for: indexPath) as! ReceipeItemCollectionViewCell
        
        return receipeItemCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
