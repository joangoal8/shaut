//
//  DietCategoryCollectionViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 16/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class DietCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dietCategoryLabel: UILabel!
    
}
