//
//  FeedPostTableViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 19/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class FeedPostTableViewCell: UITableViewCell {

    
    @IBOutlet weak var authorPicture: UIImageView!
    
    @IBOutlet weak var receipeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        authorPicture.layer.cornerRadius = authorPicture.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

