//
//  HomeShauttViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Intents
import IntentsUI

class HomeShauttViewController: UIViewController {
    
    @IBOutlet weak var helloItem: UILabel!
    
    @IBOutlet weak var homeSearchBar: UISearchBar!
    
    @IBOutlet weak var collectionDietCategoryView: UICollectionView!
    
    @IBOutlet weak var feedTableView: UITableView!
    
    @IBOutlet weak var reloadBtn: UIButton!
    
    let sharedDefaults = UserDefaults(suiteName: ShauttConstants.GROUP_ID)
    
    let testList = ["modify", "test2", "category1", "isScroll", "fitness"]
    let receipeList = ["saladAvocado", "sushiTest"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        INPreferences.requestSiriAuthorization { (status) in
            print("Siri Authorization Status - ", status)
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                print("User gave permissions for local notifications")
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(testNotification(notification:)), name: Notification.Name("testNotificationView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addSiriProduct(notification:)), name: Notification.Name(ShauttConstants.SIRI_NOTIFICATION_NAME), object: nil)
        
        self.feedTableView.register(UINib(nibName: "FeedPostTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedPostCell")
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func testNotification(notification: Notification) {
        print(" Test Method View ")
        print(notification)
    }
    
    @objc func addSiriProduct(notification: Notification) {
        print(" arriboooo o nooo ")
        
        if let shoppingBasketId = sharedDefaults?.string(forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY), let itemName = sharedDefaults?.string(forKey: ShauttConstants.SIRI_ADD_PRODUCT_NAME_KEY) {
            helloItem.text = shoppingBasketId + " " + itemName
        } else {
            helloItem.text = " He arribat almenys"
        }
        
    }
}

extension HomeShauttViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    }
}

extension HomeShauttViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return testList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dietCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DietCategoryCell", for: indexPath) as! DietCategoryCollectionViewCell
        
        dietCategoryCell.dietCategoryLabel.text = testList[indexPath.row]
        dietCategoryCell.layer.borderColor = UIColor.systemPink.cgColor
        dietCategoryCell.layer.borderWidth = 1.0
        dietCategoryCell.layer.cornerRadius = 17
        
        return dietCategoryCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(testList[indexPath.row])
    }
}

extension HomeShauttViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feedPostCell = tableView.dequeueReusableCell(withIdentifier: "FeedPostCell", for: indexPath) as! FeedPostTableViewCell
        
        feedPostCell.receipeImage.image = UIImage(named: receipeList[indexPath.row])
        
        return feedPostCell
    }

}
