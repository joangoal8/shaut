//
//  ItemDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ItemDTO {
    
    let itemId: String
    let categoryId : String
    let name: String
    let name_es: String
    var dietType: String?
    var itemDataImage: Data?
    var quantity: Int
    
    init(id: String, info: Dictionary<String,Any>, image: Data? = nil, quantity: Int = 0) {
        self.itemId = id
        self.categoryId = info["categoryId"] as! String
        self.name = info["name"] as! String
        self.name_es = info["name_es"] as! String
        if let diet = info["dietType"] {
            self.dietType = diet as? String
        }
        self.itemDataImage = image
        self.quantity = quantity
    }
    
}
