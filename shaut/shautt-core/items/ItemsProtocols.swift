//
//  ItemsProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

protocol ItemViewToPresenterInteractorProtocol: class {
        
    func retrieveCategoryItems()
}

protocol ItemPresenterToViewProtocol: class {
    
    func defineCategories(categories: [CategoryDTO])
    
    func drawItemsTable(itemsByCategories: [String:[ItemDTO]])
    
    func updateItemsTable(itemsByCategories: [String:[ItemDTO]], categoryName: String)
}

protocol ItemEntityToPresenterInteractorProtocol {
    
    func presentCategoryItems(categories: [CategoryDTO])
    
    func presentItemsTable(items: [ItemDTO])
    
    func addImages(imageId: String, data: Data?)
}

protocol AddItemViewToPresenterInteractorProtocol: class {
    
    func addItemToShoppingBasket(item: ItemDTO)
}

protocol AddItemPresenterToViewProtocol: class {
    
    func drawResult()
}

protocol AddItemEntityToPresenterInteractorProtocol: class {
    
    func updateItemToShoppingBasket(item: ItemDTO)
}
