//
//  ProductCategoriesViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProductCategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var itemPresenterInteractor: ProductItemPresenterInteractor?
    
    var categories = [CategoryDTO]()
    var filteredCategories = [CategoryDTO]()
    
    var itemsByCategories = [String:[ItemDTO]]()
    var filteredItems = [String:[ItemDTO]]()
    var expandableItems = [String:[ItemDTO]]()
    var isPreFiltered = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var customCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if itemPresenterInteractor == nil {
            self.itemPresenterInteractor = ProductItemPresenterInteractor(self)
        }
        
        self.itemPresenterInteractor?.retrieveCategoryItems()
    }
    
    @objc func handleExpandClose(button: UIButton) {
        
        let itemsList = extractedMapOfItemsRequired()
        let allCategories = extractedCategoriesRequired()
        
        var categoryDto = allCategories[button.tag]
        categoryDto.isExpanded.toggle()
        updateCategoriesRequired(categoryDto, index: button.tag)
        updateExtendedSections(itemsList, categoryDto)
    }
    
    fileprivate func updateExtendedSections(_ itemsList: [String : [ItemDTO]], _ categoryDto: CategoryDTO) {
        var newItemsExtandable = [ItemDTO]()
        if let items = itemsList[categoryDto.name] {
            if !items.isEmpty {
                if categoryDto.isExpanded {
                    itemsList[categoryDto.name]?.forEach({ (item) in
                        newItemsExtandable.append(item)
                    })
                }
                self.expandableItems[categoryDto.name] = newItemsExtandable
                customCollectionView.reloadData()
            }
        }
    }
    
    @objc func showItemsDetails(button: CustomItemButton) {
        let addItemToShoppingBasketView = Bundle.main.loadNibNamed("AddItemToShoppingBasketView", owner: nil, options: nil)?[0] as? AddItemToShoppingBasketView
        if let newView = addItemToShoppingBasketView, var itemDto = button.itemDTO {
            if itemDto.quantity == 0 {
                itemDto.quantity = 1
            }
            
            newView.setUpViewWithItemDTO(presenter: self.itemPresenterInteractor!, itemDTO: itemDto)
            view.addSubview(newView)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ItemCollectionHeader", for: indexPath) as! ItemsCategoryHeaderCollectionReusableView

        let sections = extractedCategoriesRequired()
        
        header.headerButton.setTitle(" \(sections[indexPath.section].nameEs)", for: .normal)
        header.setCustomTarget(self, action: #selector(handleExpandClose), for: .touchUpInside, section: indexPath.section)
        
        return header
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let sections = extractedCategoriesRequired()
        return sections.count
    }
    
    fileprivate func extractedCategoriesRequired() -> [CategoryDTO] {
        return (self.searchBar.text != nil && self.searchBar.text != "") ? self.filteredCategories : self.categories
    }
    
    fileprivate func updateCategoriesRequired(_ category: CategoryDTO, index: Int) {
        if (self.searchBar.text != nil && self.searchBar.text != "") {
            filteredCategories[index] = category
            return
        }
        categories[index] = category
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard
            !categories.isEmpty else {
            return 1
        }
        let sections = extractedCategoriesRequired()
        let categoryDTO = sections[section]
        return self.expandableItems[categoryDTO.name]?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollCell", for: indexPath) as! ProductCollectionViewCell
        
        let allCategories = extractedCategoriesRequired()
        
        guard
            !allCategories.isEmpty else {
            return collectionCell
        }
        guard
            !expandableItems.isEmpty else {
            return collectionCell
        }

        let categoryDto = allCategories[indexPath.section]
        let categoryItems = expandableItems[categoryDto.name]
        
        guard
            !categoryItems!.isEmpty else {
            return collectionCell
        }

        let itemDto = categoryItems![indexPath.row]
        
        if let dataImage = itemDto.itemDataImage {
            collectionCell.productButtonImage.imageView?.image = UIImage(data: dataImage)!
        } else {
            collectionCell.productButtonImage.imageView?.image = UIImage(named: "noImage")!
        }
        collectionCell.setCustomTarget(self, action: #selector(showItemsDetails), for: .touchUpInside, item: itemDto)
        
        return collectionCell
    }
    
    fileprivate func extractedMapOfItemsRequired() -> [String : [ItemDTO]] {
        return (self.searchBar.text != nil && self.searchBar.text != "") ? self.filteredItems : self.itemsByCategories
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductCategoriesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var newFilteredCatefories = [CategoryDTO]()
        var newFilteredItems = [String:[ItemDTO]]()
        expandableItems = [String:[ItemDTO]]()
        
        if (self.searchBar.text != nil && self.searchBar.text != "") {
            
            for index in categories.indices {
                let category = categories[index]
                newFilteredItems[category.name] = [ItemDTO]()
                categories[index].isExpanded = false
            }
            
            for key in itemsByCategories.keys {
                newFilteredItems[key] = itemsByCategories[key]?.filter({ (item) -> Bool in
                    return item.name_es.uppercased().contains(searchBar.text!.uppercased())
                })
                
                if newFilteredItems[key]!.count > 0 {
                    let categoryDto = categories.filter { (category) -> Bool in
                        return category.name == key
                    }.first
                    if let category = categoryDto {
                        newFilteredCatefories.append(category)
                    }
                }
            }
        }
        
        filteredItems = newFilteredItems
        filteredCategories = newFilteredCatefories
        
        self.customCollectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ProductCategoriesViewController: ItemPresenterToViewProtocol {
    
    func defineCategories(categories: [CategoryDTO]) {
        self.categories = categories
        categories.forEach { (category) in
            self.filteredItems[category.name] = [ItemDTO]()
            self.expandableItems[category.name] = [ItemDTO]()
        }
        self.customCollectionView.reloadData()
    }
    
    func drawItemsTable(itemsByCategories: [String:[ItemDTO]]) {
        self.itemsByCategories = itemsByCategories
    }
    
    func updateItemsTable(itemsByCategories: [String:[ItemDTO]], categoryName: String) {
        self.itemsByCategories = itemsByCategories
        if let items = self.itemsByCategories[categoryName],  expandableItems[categoryName] != nil {
            expandableItems[categoryName]?.removeAll()
            expandableItems[categoryName]?.append(contentsOf: items)
        }
        self.customCollectionView.reloadData()
    }
}
