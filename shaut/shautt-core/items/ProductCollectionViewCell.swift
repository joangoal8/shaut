//
//  ProductCollectionViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productButtonImage: CustomItemButton!
    
    func setCustomTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event, item: ItemDTO?) {
        productButtonImage.itemDTO = item
        productButtonImage.addTarget(target, action: action, for: controlEvents)
    }
}
