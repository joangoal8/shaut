//
//  CategoryDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct CategoryDTO {
    
    let categoryId: String
    let name: String
    let nameEs: String
    var isExpanded = false
    
    init(id: String, data: Dictionary<String,Any>) {
        self.categoryId = id
        self.name = data["name"] as! String
        self.nameEs = data["name_es"] as! String
    }
}
