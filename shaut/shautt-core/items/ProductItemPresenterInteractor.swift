//
//  ProductItemPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift

class ProductItemPresenterInteractor: ItemViewToPresenterInteractorProtocol {
    
    weak var view: ItemPresenterToViewProtocol?
    weak var addItemView: AddItemPresenterToViewProtocol?
    var categories = [CategoryDTO]()
    var itemsCategoryData = [String:[ItemDTO]]()
    var images = [String:Data]()
    var productCategoryFirestoreDataProvider: ProductCategoryFirestoreDataProvider?
    let updateItemService: UpdateItemSBService
    
    init(_ collectionViewController: ItemPresenterToViewProtocol) {
        self.view = collectionViewController
        updateItemService = InjectionEngine.updateItemSBService
        updateItemService.updateItemProductItemDelegate = self
    }
    
    func retrieveCategoryItems() {
        let itemFireStore = getProductFireStoreDataProvider()
        itemFireStore.retrieveCategories()
    }
    
    fileprivate func getProductFireStoreDataProvider() -> ProductCategoryFirestoreDataProvider {
        if let productItemFirestore = productCategoryFirestoreDataProvider {
            return productItemFirestore
        }
        productCategoryFirestoreDataProvider = ProductCategoryFirestoreDataProvider(self)
        return productCategoryFirestoreDataProvider!
    }
    
}

extension ProductItemPresenterInteractor: ItemEntityToPresenterInteractorProtocol {
    
    func presentCategoryItems(categories: [CategoryDTO]) {
        self.categories = categories
        self.categories.sort { (category1, category2) -> Bool in
            return category1.name < category2.name
        }
        let itemFireStore = getProductFireStoreDataProvider()
        itemFireStore.retrieveItems()
        view?.defineCategories(categories: self.categories)
    }
    
    func presentItemsTable(items: [ItemDTO]) {
        let itemCategoriesData = categories.reduce([String:[ItemDTO]]()) { (result, category) -> [String:[ItemDTO]] in
            var newResult = result
            let itemsInCategory = items.filter { (item) -> Bool in
                return category.categoryId.elementsEqual(item.categoryId)
            }
            newResult[category.name] = itemsInCategory
            return newResult
        }
        self.itemsCategoryData = itemCategoriesData
        view?.drawItemsTable(itemsByCategories: self.itemsCategoryData)
    }
    
    func addImages(imageId: String, data: Data?) {
        //view?.drawItemsTable(itemsByCategories: self.itemsCategoryData)
    }
}

extension ProductItemPresenterInteractor: AddItemViewToPresenterInteractorProtocol {
    
    func addItemToShoppingBasket(item: ItemDTO) {
        if item.quantity > 0, let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
            let addItemService = InjectionEngine.updateItemSBService
            addItemService.updateItemProductItemDelegate = self
            addItemService.addItemToSB(itemDto: item, shoppingBasketId: shoppingBasketId)
        }
    }
}

extension ProductItemPresenterInteractor: UpdateItemProductItemDelegate {
    
    func updateItemToShoppingBasket(item: ItemDTO) {
        let categoryId = item.categoryId
        let categoryDto = categories.filter { (category) -> Bool in
            return category.categoryId == categoryId
        }.first
        if let category = categoryDto, var items = itemsCategoryData[category.name] {
            for index in items.indices {
                if (item.itemId == items[index].itemId) {
                    items[index] = item
                }
            }
            self.itemsCategoryData[category.name]?.removeAll()
            self.itemsCategoryData[category.name]?.append(contentsOf: items)
            view?.updateItemsTable(itemsByCategories: self.itemsCategoryData, categoryName: category.name)
        }
    }
    
}
