//
//  ProductCategoryFirestoreDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class ProductCategoryFirestoreDataProvider {

    let dataBase = Firestore.firestore()
    let storage = Storage.storage()

    weak var productItemPresenterInteractor: ProductItemPresenterInteractor?

    init(_ tableViewPresenterInteractor: ProductItemPresenterInteractor) {
        self.productItemPresenterInteractor = tableViewPresenterInteractor
    }
    
    func retrieveCategories() {
        var categories = [CategoryDTO]()
        dataBase.collection("categories").getDocuments() { (snapshot, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
                return
            }
            
            for document in snapshot!.documents {
                let data = document.data() as Dictionary<String,Any>
                let category = CategoryDTO(id: document.documentID, data: data)
                categories.append(category)
            }
            
            self.productItemPresenterInteractor?.presentCategoryItems(categories: categories)
        }
    }
    
    func retrieveItems() {
        var items = [ItemDTO]()
        dataBase.collection("items").getDocuments() { (snapshot, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
            }
            
            for document in snapshot!.documents {
                
                let imageRef = self.storage.reference().child(
                "items/images/\(document.documentID).jpg")
                imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    
                    if let errorResult = error {
                        print(errorResult.localizedDescription)
                        self.productItemPresenterInteractor?.addImages(imageId: document.documentID, data: nil)
                    } else {
                        self.productItemPresenterInteractor?.addImages(imageId: document.documentID, data: data!)
                    }
                }
                let item = ItemDTO(id: document.documentID, info: document.data() as Dictionary<String,Any>)
                items.append(item)
            }
            self.productItemPresenterInteractor?.presentItemsTable(items: items)
        }
    }
}
