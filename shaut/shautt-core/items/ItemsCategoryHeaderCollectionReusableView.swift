//
//  ItemsCategoryHeaderCollectionReusableView.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 14/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ItemsCategoryHeaderCollectionReusableView: UICollectionReusableView {
        
    static let identifier = "ItemCollectionHeader"
    
    @IBOutlet weak var headerButton: UIButton!
    
    func setCustomTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event, section: Int) {
        layer.cornerRadius = 10
        headerButton.bounds = bounds
        headerButton.layer.cornerRadius = 10
        headerButton.tag = section
        headerButton.addTarget(target, action: action, for: controlEvents)
    }

}
