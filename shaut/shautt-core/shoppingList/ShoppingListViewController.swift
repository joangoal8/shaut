//
//  ShoppingListViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ShoppingListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var customTableView: UITableView!
    
    var shopListPresenterInteractor: ShoppingListViewToPresenterInteractorProtocol?
    var shoppingList = [ShopItemListDTO]()
    var totalAmountShoppingBasket: Decimal = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if shopListPresenterInteractor == nil {
            self.shopListPresenterInteractor = ShopListPresenterInteractor(self)
        }
        
        self.shopListPresenterInteractor?.retrieveShoppingList()
        
        buyNowButton.layer.cornerRadius = buyNowButton.layer.frame.height/4
        buyNowButton.layer.masksToBounds = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return shoppingList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ShopItem", for: indexPath) as! ShopItemTableViewCell
        let itemDto = shoppingList[indexPath.row]

        itemCell.itemName.text = itemDto.name_es
        
        itemCell.itemQuantity.text = "\(itemDto.quantity)"
            
        if let dataImage = itemDto.itemDataImage {
            itemCell.itemImage.image = UIImage(data: dataImage)!
        } else {
            itemCell.itemImage.image = UIImage(named: "noImage")!
        }
        
        return itemCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(shoppingList[indexPath.row])
    }

    
    @IBAction func refreshShoppingBasket(_ sender: Any) {
        if shopListPresenterInteractor == nil {
            self.shopListPresenterInteractor = ShopListPresenterInteractor(self)
        }
        
        self.shopListPresenterInteractor?.retrieveShoppingList()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProviderView" {
            let providerView = segue.destination as! ProvidersViewController
            providerView.shoppingList = shoppingList
        }
    }
    
}

extension ShoppingListViewController: ShoppingListPresenterToViewProtocol {
    
    func drawShoppingListTable(items: [ShopItemListDTO]) {
        self.shoppingList = items
        self.customTableView.reloadData()
    }
    
}

extension ShoppingListViewController {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Borrar") { (action, rowView, handler) in
            self.shopListPresenterInteractor?.removeItemFromShoppingList(shopItem: self.shoppingList[indexPath.row])
            self.shoppingList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        let editAction = UIContextualAction(style: .normal, title: "Editar") { (action, rowView, handler) in
            print("handler Editar")
            let shopItem = self.shoppingList[indexPath.row]
            let dictionary: Dictionary<String, Any> = [
                "name": shopItem.name,
                "name_es": shopItem.name_es,
                "categoryId": shopItem.categoryId,
                "dietType": shopItem.dietType as Any
            ]
            let item = ItemDTO(id: shopItem.itemId, info: dictionary, image: shopItem.itemDataImage, quantity: shopItem.quantity)
            let addItemToShoppingBasketView = Bundle.main.loadNibNamed("AddItemToShoppingBasketView", owner: nil, options: nil)?[0] as? AddItemToShoppingBasketView
            
            if let newView = addItemToShoppingBasketView {
                newView.setUpEditableViewWithItemDTO(presenter: self.shopListPresenterInteractor! as! ShopListPresenterInteractor, itemDTO: item)
                self.view.addSubview(newView)
            }
        }
        editAction.backgroundColor = .orange
        
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        
        return swipeAction
    }
}
