//
//  ShopListFirebaseDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase
import KeychainSwift

class ShopListFirebaseDataProvider {

    let dataBase = Firestore.firestore()
    let storage = Storage.storage()
    let functions = Functions.functions(region : "europe-west6")
    let shopListEntityToPresenterInteractorProtocol: ShoppingListEntityToPresenterInteractorProtocol
    
    init(_ tableViewPresenterInteractor: ShoppingListEntityToPresenterInteractorProtocol) {
        self.shopListEntityToPresenterInteractorProtocol = tableViewPresenterInteractor
    }
    
    func retrieveShoppingList() {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
            retrieveShopListByShoppingBasketId(shoppingBasketId)
        } else {
            if let dataUser = KeychainSwift().getData("current_user") {
                do {
                    let decoder = JSONDecoder()
                    let user = try decoder.decode(UserLogin.self, from: dataUser)
                    retrieveShopListByUserEmail(email: user.email)
                } catch {
                    print(error)
                }
            }
        }
    }
    
    fileprivate func retrieveShopListByUserEmail(email: String) {
        let userRef = dataBase.collection("users").document(email)
        userRef.getDocument { (docSnapshot, error) in
            if let shoppingBasketID = docSnapshot?.get("shoppingBasketId") {
                self.retrieveShopListByShoppingBasketId(shoppingBasketID as! String)
            }
        }
    }
    
    fileprivate func retrieveShopListByShoppingBasketId(_ shoppingBasketID: String) {
        functions.httpsCallable("getShoppingBasket").call(["id" : shoppingBasketID]) { (result, error) in
            if let err = error as NSError? {
                if err.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: err.code)
                    let message = err.localizedDescription
                    let details = err.userInfo[FunctionsErrorDetailsKey]
                    print(code!)
                    print(message)
                    if let detail = details {
                        print(detail)
                    }
                }
                print(err.localizedDescription)
                return
            }
            if let data = (result?.data as? Array<Dictionary<String, Any>>){
                var shopListItems = [ShopItemListDTO]()
                data.forEach { (item) in
                    shopListItems.append(ShopItemListDTO(data: item))
                }
                self.shopListEntityToPresenterInteractorProtocol.addItemsAndPresentShoppingListTable(items: shopListItems)
                self.extractProductsWithImagesFromShoppingBasket(shopListItems)
            }
        }
    }
    
    func extractProductsWithImagesFromShoppingBasket(_ items: [ShopItemListDTO]) {
        for item in items {
            
            let itemId = item.itemId
            let imageRef = self.storage.reference().child(
                "items/images/\(itemId).jpg")
            imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let errorResult = error {
                    print(errorResult.localizedDescription)
                    self.shopListEntityToPresenterInteractorProtocol.addImages(imageId: itemId, data: nil)
                } else {
                    self.shopListEntityToPresenterInteractorProtocol.addImages(imageId: itemId, data: data!)
                }
            }
        }
    }
}
