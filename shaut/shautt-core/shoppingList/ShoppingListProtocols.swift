//
//  ShoppingListProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol ShoppingListViewToPresenterInteractorProtocol: class {
    
    func retrieveShoppingList()
    
    func removeItemFromShoppingList(shopItem: ShopItemListDTO)
    
    func editItemShoppingList(item: ItemDTO)
}

protocol ShoppingListPresenterToViewProtocol: class {
    
    func drawShoppingListTable(items: [ShopItemListDTO])
}

protocol ShoppingListEntityToPresenterInteractorProtocol {
    
    func addItemsAndPresentShoppingListTable(items: [ShopItemListDTO])
    
    func addImages(imageId: String, data: Data?)
}
