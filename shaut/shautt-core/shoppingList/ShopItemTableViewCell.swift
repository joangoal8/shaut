//
//  ShopItemTableViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 18/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ShopItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var hStack: UIStackView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        hStack.translatesAutoresizingMaskIntoConstraints = false
        hStack.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        itemQuantity.layer.masksToBounds = true
        itemQuantity.layer.cornerRadius = 8
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
