//
//  ProviderPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift

class ProviderPresenterInteractor: ProviderViewToPresenterInteractorProtocol {
    
    weak var view: ProviderPresenterToViewProtocol?
    var shopProviders = [ShopProviderDTO]()
    let providerEngine = ProvidersEngine()
    var supermarketProviders = [String:SupermarketProvider]()
    
    init(_ tableViewController: ProviderPresenterToViewProtocol) {
        self.view = tableViewController
    }
    
    func retrieveProviders(shopList: [ShopItemListDTO]) {
        print(" ENTRO RETRIEVEEE PROV ")
        if let shoppingProvidersStored = KeychainSwift().getData("shopping_providers") {
            do {
                let decoder = JSONDecoder()
                let shoppingProviders = try decoder.decode([ShopProviderDTO].self, from: shoppingProvidersStored)
                shopProviders = shoppingProviders
                print(" INTO SHOP PROV RETRIEVEEE PROV ")
                self.getShoppingBasketProviderPrice(shopList: shopList, shopProviders: shopProviders)
            } catch {
                print("ERROR")
                print(error.localizedDescription)
            }
        } else {
            ProviderInitConfigurationManager().retrieveProviders { (extractedShopProviders) in
                self.shopProviders = extractedShopProviders
                self.getShoppingBasketProviderPrice(shopList: shopList, shopProviders: self.shopProviders)
            }
        }
    }
    
    private func getShoppingBasketProviderPrice(shopList: [ShopItemListDTO], shopProviders: [ShopProviderDTO]) {
        
        print(" GETTING SHOP VALUEEE ")
        if let postalCode = KeychainSwift().get("postal_code") {
            
            print(" POSTAL_CODE STORED  :: \(postalCode)")
            shopProviders.forEach { (shopProvider) in
                print(" Check Shop Provider ID :: \(shopProvider)")
                if let deliberryStoreId = shopProvider.deliberryProviderId, !deliberryStoreId.isEmpty {
                    print(" Check Deliberry Provider ID :: \(deliberryStoreId)")
                    let itemsDeliveryInfo = extractItemDeliveryProvider(shopProvider: shopProvider, shopList: shopList)
                    providerEngine.shopDeliberryBasketValue(storeId: deliberryStoreId, postalCode: postalCode, itemsInfo: itemsDeliveryInfo) { (deliberryDeliveryProvider) in
                        print(" ADDING DELIBERY  :: \(deliberryDeliveryProvider)")
                        self.addDeliveryProvider(shopProviderName: shopProvider.name, deliberryDeliveryProvider: deliberryDeliveryProvider)
                    }
                }
                /*
                 if let lolamarketStoreId = shopProvider.lolamarketProviderId {
                 var itemsDeliveryInfo = [ItemDeliveryProviderInformation]()
                 shopList.forEach { (shopItem) in
                 itemsDeliveryInfo.append(ItemDeliveryProviderInformation(itemId: shopItem.lolamarketItemId!, quantity: shopItem.quantity))
                 }
                 providerEngine.shopLolamarketBasketValue(storeId: lolamarketStoreId, postalCode: postalCode, itemsInfo: itemsDeliveryInfo) { (lolamarketDeliveryProvider) in
                 self.addDeliveryProvider(shopProviderName: shopProvider.name, deliberryDeliveryProvider: lolamarketDeliveryProvider)
                 }
                 }
                 */
            }
        }
    }
    
    private func extractItemDeliveryProvider(shopProvider: ShopProviderDTO, shopList: [ShopItemListDTO]) -> [ItemDeliveryProviderInformation] {
        var itemsDeliveryInfo = [ItemDeliveryProviderInformation]()
        
        switch shopProvider.name.lowercased() {
        case SuperProvider.MERCADONA.rawValue.lowercased():
            shopList.forEach { (shopItem) in
                itemsDeliveryInfo.append(ItemDeliveryProviderInformation(itemId: shopItem.deliberryMercadonaItemId!, quantity: shopItem.quantity))
            }
            return itemsDeliveryInfo
        case SuperProvider.CAPRABO.rawValue.lowercased():
            shopList.forEach { (shopItem) in
                itemsDeliveryInfo.append(ItemDeliveryProviderInformation(itemId: shopItem.deliberryCapraboItemId!, quantity: shopItem.quantity))
            }
            return itemsDeliveryInfo
        case SuperProvider.DIA.rawValue.lowercased():
            shopList.forEach { (shopItem) in
                itemsDeliveryInfo.append(ItemDeliveryProviderInformation(itemId: shopItem.deliberryDiaItemId!, quantity: shopItem.quantity))
            }
            return itemsDeliveryInfo
        default:
            return itemsDeliveryInfo
        }
    }
    
    fileprivate func addDeliveryProvider(shopProviderName: String, deliberryDeliveryProvider: DeliveryProvider) {
        if self.supermarketProviders[shopProviderName] != nil {
            self.supermarketProviders[shopProviderName]?.appendDeliveryProvider(newElement: deliberryDeliveryProvider)
        } else {
            let supermarketProvider = SupermarketProvider(name: shopProviderName)
            supermarketProvider.appendDeliveryProvider(newElement: deliberryDeliveryProvider)
            self.supermarketProviders[shopProviderName] = supermarketProvider
        }
    }
}
