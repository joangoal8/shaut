//
//  SupermarketProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class SupermarketProvider {
    
    let name: String
    var price: Decimal?
    private var deliveryProviders = [DeliveryProvider]()
    
    init(name: String) {
        self.name = name
    }
    
    func appendDeliveryProvider(newElement: DeliveryProvider) {
        deliveryProviders.append(newElement)
        calculatePrice()
    }
    
    private func calculatePrice() {
        price = deliveryProviders.map {$0.price}.reduce(Decimal(10000)) { (minPrice, nextPrice) -> Decimal in
            if (nextPrice < minPrice) {
                return nextPrice
            }
            return minPrice
        }
    }
    
    func getDeliveryProviders() -> [DeliveryProvider] {
        return deliveryProviders
    }
}

enum SuperProvider: String {
    
    case MERCADONA, CAPRABO, DIA, LIDL, CARREFOUR
}
