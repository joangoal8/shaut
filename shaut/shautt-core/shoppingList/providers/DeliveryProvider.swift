//
//  DeliveryProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class DeliveryProvider {
    
    let name: String
    let price: Decimal
    
    init(brand: DeliveryBrand, price: String) {
        self.name = brand.rawValue
        self.price = Decimal(string: price)!
    }
}

enum DeliveryBrand: String {
    
    case DELIBERRY, LOLAMARKET, ULABOX
}
