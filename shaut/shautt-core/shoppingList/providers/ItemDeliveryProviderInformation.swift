//
//  ItemsDeliveryProviderInformation.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ItemDeliveryProviderInformation: Codable {
    let itemId: String
    let quantity: Int
    
    init(itemId: String, quantity: Int) {
        self.itemId = itemId
        self.quantity = quantity
    }
}
