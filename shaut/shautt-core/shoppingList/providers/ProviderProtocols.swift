//
//  ProviderProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol ProviderViewToPresenterInteractorProtocol: class {
    
    func retrieveProviders(shopList: [ShopItemListDTO])
}

protocol ProviderPresenterToViewProtocol: class {
    
}

protocol ProviderEntityToPresenterInteractorProtocol {
    
}
