//
//  ProvidersViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 31/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProvidersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var providerTableView: UITableView!
    
    var providerPresenterInteractor: ProviderViewToPresenterInteractorProtocol?
    
    var shoppingList = [ShopItemListDTO]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(shoppingList)
        
        if providerPresenterInteractor == nil {
            self.providerPresenterInteractor = ProviderPresenterInteractor(self)
        }
        
        providerPresenterInteractor?.retrieveProviders(shopList: shoppingList)
        // Do any additional setup after loading the view.
        providerTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let providerCell = tableView.dequeueReusableCell(withIdentifier: "ProviderCell", for: indexPath) as! ProviderTableViewCell
        
        providerCell.providerName.text = "Mercadona"
        providerCell.providerPrice.text = "25€"
        providerCell.providerImage.image = UIImage(named: "noImage")!
        return providerCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("click")
        let application = UIApplication.shared
        let deliberryAppPath = "fb1594065867473346://"
        
        let deliberryAppURL = URL(string: deliberryAppPath)!
        
        let deliberryHomePageURL = URL(string: "https://www.deliberry.com/")!
        
        if application.canOpenURL(deliberryAppURL) {
            application.open(deliberryAppURL, options: [:], completionHandler: nil)
        } else {
            application.open(deliberryHomePageURL)
        }
    }

    @IBAction func backToLastView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProvidersViewController: ProviderPresenterToViewProtocol {
    
}
