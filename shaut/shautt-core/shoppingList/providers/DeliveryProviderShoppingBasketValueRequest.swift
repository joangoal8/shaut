//
//  DeliveryProviderShoppingBasketValueRequest.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct DeliveryProviderShoppingBasketValueRequest: Encodable {
    
    let storeId: String
    let postalCode: String
    let itemsInformation: [ItemDeliveryProviderInformation]
    
    init(storeId: String, postalCode: String, itemsInformation: [ItemDeliveryProviderInformation]) {
        self.storeId = storeId
        self.postalCode = postalCode
        self.itemsInformation = itemsInformation
    }
}
