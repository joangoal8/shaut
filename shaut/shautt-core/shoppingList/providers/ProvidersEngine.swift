//
//  ProvidersEngine.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class ProvidersEngine {
    
    let functions = Functions.functions(region : "europe-west6")
    
    func shopDeliberryBasketValue(storeId: String, postalCode: String, itemsInfo: [ItemDeliveryProviderInformation], completionHandler: @escaping (DeliveryProvider) -> Void) {
        
        let deliveryShoppingBasketValueRequest = DeliveryProviderShoppingBasketValueRequest(storeId: storeId, postalCode: postalCode, itemsInformation: itemsInfo)
        
        print(" DELIVERY SHOP VALUE REQUEST IS  :: \(deliveryShoppingBasketValueRequest)")
        
        guard let shoppingBasketRequestData = try? JSONEncoder().encode(deliveryShoppingBasketValueRequest) else {
            return
        }
        
        guard let dictionaryData = try? JSONSerialization.jsonObject(with: shoppingBasketRequestData, options: .allowFragments) as? Dictionary<String, Any> else {
            return
        }
        
        print(dictionaryData)
        
        functions.httpsCallable("deliberrryShoppingCart").call(dictionaryData) { (result, error) in
            if let err = error as NSError? {
                if err.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: err.code)
                    let message = err.localizedDescription
                    let details = err.userInfo[FunctionsErrorDetailsKey]
                    print(code!)
                    print(message)
                    if let detail = details {
                        print(detail)
                    }
                }
                print(err.localizedDescription)
                return
            }
            if let data = result {
                print("Shopping Basket Value")
                print(data.data)
            }
        }
    }
    
    func shopLolamarketBasketValue(storeId: String, postalCode: String, itemsInfo: [ItemDeliveryProviderInformation], completionHandler: @escaping (DeliveryProvider) -> Void) {
        
    }
}
