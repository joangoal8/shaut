//
//  ShopListPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import KeychainSwift

class ShopListPresenterInteractor {
    
    weak var view: ShoppingListPresenterToViewProtocol?
    var items = [ShopItemListDTO]()
    var images = [String:Data]()
    var shopListFirebaseDataProvider: ShopListFirebaseDataProvider?
    let updateItemService: UpdateItemSBService
    
    init(_ tableViewController: ShoppingListPresenterToViewProtocol) {
        self.view = tableViewController
        updateItemService = InjectionEngine.updateItemSBService
        updateItemService.updateItemToShoppingListDelegate = self
    }
    
    fileprivate func getShopListFirebaseDataProvider() -> ShopListFirebaseDataProvider {
        if let shopListFirebaseProvider = shopListFirebaseDataProvider {
            return shopListFirebaseProvider
        }
        shopListFirebaseDataProvider = ShopListFirebaseDataProvider(self)
        return shopListFirebaseDataProvider!
    }
    
}

extension ShopListPresenterInteractor: ShoppingListViewToPresenterInteractorProtocol {
    
    func retrieveShoppingList() {
        let shopListFBProvider = getShopListFirebaseDataProvider()
        shopListFBProvider.retrieveShoppingList()
    }
    
    func removeItemFromShoppingList(shopItem: ShopItemListDTO) {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
            let dictionary: Dictionary<String, Any> = [
                "name": shopItem.name,
                "name_es": shopItem.name_es,
                "categoryId": shopItem.categoryId,
                "dietType": shopItem.dietType as Any
            ]
            let item = ItemDTO(id: shopItem.itemId, info: dictionary, image: shopItem.itemDataImage, quantity: shopItem.quantity)
            updateItemService.removeItemToSB(itemDto: item, shoppingBasketId: shoppingBasketId)
        }
    }
    
    func editItemShoppingList(item: ItemDTO) {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
           updateItemService.editItemToSB(itemDto: item, shoppingBasketId: shoppingBasketId)
        }
    }
}

extension ShopListPresenterInteractor: ShoppingListEntityToPresenterInteractorProtocol {
        
    func addItemsAndPresentShoppingListTable(items: [ShopItemListDTO]) {
        self.items = items
        view?.drawShoppingListTable(items: items)
    }
    
    func addImages(imageId: String, data: Data?) {
        images[imageId] = data
        if (items.count > 0 && items.count == images.count) {
            var itemsWithImages = [ShopItemListDTO]()
            for i in 0...items.count - 1 {
                var item = items[i]
                item.itemDataImage = images[item.itemId]
                itemsWithImages.append(item)
            }
            self.items = itemsWithImages
            view?.drawShoppingListTable(items: items)
        }
    }
}

extension ShopListPresenterInteractor: UpdateItemToShoppingListDelegate {
    
    func updateShoppingBasket(items: [ShopItemListDTO]) {
        self.items = items
        let shopListFBProvider = getShopListFirebaseDataProvider()
        shopListFBProvider.extractProductsWithImagesFromShoppingBasket(items)
        view?.drawShoppingListTable(items: items)
    }
}
