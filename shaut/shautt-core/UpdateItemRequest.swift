//
//  AddItemRequestDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 24/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct UpdateItemRequest: Encodable {
    
    static let DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
    
    let id: String
    let itemInfo: ItemInfo
    let operationInfo: OperationInfo
    
    init(id: String, itemInfo: ItemInfo, operationInfo: OperationInfo) {
        self.id = id
        self.itemInfo = itemInfo
        self.operationInfo = operationInfo
    }
}

struct ItemInfo: Encodable {
    
    let itemId: String
    let quantity: Int
    let lastUpdate: String
    
    init(itemId: String, quantity: Int) {
        self.itemId = itemId
        self.quantity = quantity
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = UpdateItemRequest.DATE_FORMAT
        lastUpdate = dateFormatterGet.string(from: Date())
    }
}

struct OperationInfo: Encodable {
    
    let operation: String
    var newItemInfo: ItemInfo?
    
    init(operationAction: OperationAction) {
        self.operation = operationAction.rawValue
    }
    
}

enum OperationAction: String {
    case ADD, REMOVE, EDIT
}
