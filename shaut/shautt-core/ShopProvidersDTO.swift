//
//  ShopProvidersDTO.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ShopProviderDTO: Codable {
    
    let name: String
    let url: String
    var deliberryProviderId: String?
    var lolamarketProviderId: String?
    
    init(data: Dictionary<String,Any>) {
        self.name = data["name"] as! String
        self.url =  data["url"] as! String
        if let deliberryProvId = data["deliberryProviderId"] as? String {
            self.deliberryProviderId = deliberryProvId
        }
        if let lolaMarketProvId = data["lolamarketProviderId"] as? String {
            self.lolamarketProviderId = lolaMarketProvId
        }
    }
    
}
