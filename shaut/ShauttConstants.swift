//
//  ShautConstants.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 13/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ShauttConstants {
    
    static let GROUP_ID = "group.com.jgoal.ios.shaut"
    static let KEYCHAIN_SHOOPINGBASKETID_KEY = "user_shoppingBasketId"
    static let USER_DEFAULTS_SHOOPINGBASKETID_KEY = "user_shoppingBasketId"
    static let SIRI_NOTIFICATION_NAME = "addProduct"
    static let SIRI_ADD_PRODUCT_NAME_KEY = "siri_addproduct_name"
}
