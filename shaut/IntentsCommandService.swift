//
//  IntentsCommandService.swift
//  shauttIntentKit
//
//  Created by Joan Gómez Álvarez on 13/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class IntentsCommandService: AddItemBySearchNameCommandProtocolService {
    
    static let sharedInstance = IntentsCommandService()
    
    let functions = Functions.functions(region : "europe-west6")
    
    func addItemByNameToSB(itemName: String, shoppingBasketId : String, completionHandler: @escaping (String?, Error?) -> Void) {
        let dictionaryReq: [String: String] = [
            "id" : shoppingBasketId,
            "name": itemName,
            "language": "es"
        ]
        
        functions.httpsCallable("addItemByName").call(dictionaryReq) { (result, error) in
           if let err = error as NSError? {
                if err.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: err.code)
                    let message = err.localizedDescription
                    let details = err.userInfo[FunctionsErrorDetailsKey]
                    print(code!)
                    print(message)
                    if let detail = details {
                        print(detail)
                    }
                }
                print(err.localizedDescription)
                completionHandler(nil, err)
            }
            if let data = (result?.data as? Dictionary<String, Any>){
                completionHandler(data["name_es"] as? String, nil)
            }
        }
    }
    
}
