//
//  IntentHandler.swift
//  shauttIntents
//
//  Created by Joan Gómez Álvarez on 11/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Intents

class IntentHandler: INExtension {}

extension IntentHandler: INAddTasksIntentHandling {
    
    func handle(intent: INAddTasksIntent, completion: @escaping (INAddTasksIntentResponse) -> Void) {
        
        guard let task = intent.taskTitles else {
            completion(INAddTasksIntentResponse(code: .failure, userActivity: nil))
            return
        }
        
        let title = task.description
        
        IntentsManager.sharedInstance.add(task: title) { (itemName, error) in
             
            if let err = error {
                print(err.localizedDescription)
                completion(INAddTasksIntentResponse(code: .failure, userActivity: nil))
                return
            }
            
            guard let name = itemName else {
                completion(INAddTasksIntentResponse(code: .failure, userActivity: nil))
                return
            }
            
            completion(INAddTasksIntentResponse(code: .success, userActivity: NSUserActivity(activityType: name)))
        }
        
    }
    
}
